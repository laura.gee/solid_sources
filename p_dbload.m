function fs = p_dbload(db,fields)

% load data saved in database return in structure
% 
% fs = p_dbload(db,fields)

try
    dbdir = getpref('solid','dbdir');
catch
    dbdir = fullfile(cd,'db');
end

if not(exist('db','var'))
    db = 'restin';
end
if not(exist('fields','var'))
    fields = 'all';
end

if ischar(fields)
    if strcmp(fields,'all')
        fields = p_dbload(db,'list');
        fields = {fields.name};
    elseif strcmp(fields,'list')
        fs = dir(fullfile(dbdir,[db ,'*.mat']));
        [fs.name] = rep2struct(regexprep({fs.name},'.mat$',''));
        [fs.name] = rep2struct(regexprep({fs.name},['^' db '_' ],''));
        return
    else
        fields = {fields};
    end
end

fields = unique(['name' fields]);

for i = 1:numel(fields)
    load(fullfile(dbdir,[db '_' fields{i} '.mat']))
end


str = 'fs = struct(';
for i = 1:numel(fields)
    str = [str '''' fields{i} ''', ' fields{i} ','];
    if i == numel(fields)
        str(end:end+1) = ');';
    end
end
eval(str);

% reorder fields so they match dimensions of the structure
fields = fieldnames(fs);
fields(strcmp(fields,'name')) = [];
ii = [];
for i_f = 1:numel(fields)
    for i_dim = 1:ndims(fs)
        tmpfs = permute(fs,[i_dim setdiff(1:ndims(fs),i_dim)]);
        if isnumeric([tmpfs(1,1,1,1,1,1,1,1).(fields{i_f})])
            ii(i_dim,i_f) = numel(unique([tmpfs(:,1,1,1,1,1,1,1).(fields{i_f})]));
        elseif isstruct([tmpfs(1,1,1,1,1,1,1,1).(fields{i_f})])
            try
                ii(i_dim,i_f) = numel(uniquestruct([tmpfs(:,1,1,1,1,1,1,1).(fields{i_f})]));
            end
        elseif iscell(tmpfs(1,1,1,1,1,1,1,1).(fields{i_f}))
            tmp = {};
            for i = 1:size(tmpfs,1)
                tmp{i} = [tmpfs(i).(fields{i_f}){:}];
            end
            ii(i_dim,i_f) = numel(unique(tmp));
        else
            ii(i_dim,i_f) = numel(unique({tmpfs(:,1,1,1,1,1,1,1).(fields{i_f})}));
        end
    end
end
odr = {'name'};
for i = 1:size(ii,1)
    tmp = fields(ii(i,:) > 1);
    odr(end+1:end+numel(tmp)) = tmp;
end
if numel(odr) < numel(fields)+1
    toadd = setdiff(fields,odr);
    odr(end+1:end+numel(toadd)) = toadd;
end
fs = orderfields(fs,odr);

return

