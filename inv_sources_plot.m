
p_setpath
load layout

fs = p_dbload('restin',{'name','label','headmodel','ds','ctxsm','grad','hm','ctxsm_inflated'});
load(fullfile(datadir,'alldata_50timepoints.mat'))
alldata = reshape(alldata, [248,50*200,89]);
%%
alldata = reshape(alldata, [248, 200, 50, 89]);
lfdir = fullfile(datadir,'lf_32k');

%lfsingdir = fullfile(lfdir,['lf_32k_' fs(1).ds]);
%lfsing = leadfield(1); --> inutile
amp = 10;
t_min = -.2;
t_max = 1;
n_pts = 200;
n_source = 8004;
%%
i_suj = 2;
i_s = 1; %1:size(fs(1).ctxsm.pos,1)
redisp(i_s)
ntris = [2 5 10 20 30 50];
for i_ntri = 1:numel(ntris)
  n_tri = ntris(i_ntri);
  %%
  % créer une data structure de données "timelocked"

  % load le leadfield precomputé pour un sujet au hasard
  load(fullfile(lfdir,['lf_32k_' fs(i_suj).ds '.mat']))
  data = [];
  data.label = fs(i_suj).label;
  data.grad = fs(i_suj).grad;
  fs(i_suj).dippos = fs(i_suj).ctxsm.pos(i_s,:);
  fs(i_suj).dipmom = fs(i_suj).ctxsm.nrm(i_s,:)';
  fs(i_suj).dipmom = fs(i_suj).dipmom ./ norm(fs(i_suj).dipmom);
  % take leadfield
  fs(i_suj).lf = leadfield.leadfield{i_s}(chnb(fs(i_suj).label,leadfield.label),:);


  lftopo1 = fs(i_suj).lf * fs(i_suj).dipmom * amp;

  for i_tri = 1:n_tri


    data.trial{i_tri} = alldata(:, 1:n_pts, i_tri, i_suj);
    data.trial{i_tri}(:, 1:n_pts) = data.trial{i_tri}(:, 1:n_pts) + lftopo1 ;
    data.time{i_tri} = linspace(t_min, t_max, n_pts);    %f_e=508.6275Hz


  end


  data.dimord = 'chan_time';
  %data.avg = lftopo1;

  cfg = [];
  cfg.covariance = 'yes';
  cfg.covariancewindow = [-.2 1]; %it will calculate the covariance matrix
  % on the timepoints that are
  % before the zero-time point in the
  % trials
  tlck = ft_timelockanalysis(cfg, data);
  %tlck.grad = fs(1).grad;

  %%
  %inverse model


  cfg               = [];
  cfg.method        = 'mne';
  cfg.sourcemodel   = leadfield;
  cfg.headmodel     = fs(i_suj).headmodel;
  cfg.mne.prewhiten = 'yes';
  cfg.mne.lambda    = 3;
  cfg.mne.scalesourcecov = 'yes';

  source1          = ft_sourceanalysis(cfg,tlck);

  %%
  %visualisation

  figure(1); clf

  m = mean(source1.avg.pow(:,1:n_pts), 2);
  ft_plot_mesh(source1, 'vertexcolor', m, 'edgecolor','k');
  view([180 0]); h = light; set(h, 'position', [0 1 0.2]); lighting gouraud; material dull
  hold on
  % quiver3(fs(i_suj).dippos(1,1),fs(i_suj).dippos(1,2),fs(i_suj).dippos(1,3),fs(i_suj).dipmom(1),fs(i_suj).dipmom(2),fs(i_suj).dipmom(3),.1,'color',colors('red'),'linewidth',1,'maxheadsize',.5,'autoscale','on')
  quiver3(fs(i_suj).ctxsm.pos(i_s,1),fs(i_suj).ctxsm.pos(i_s,2),fs(i_suj).ctxsm.pos(i_s,3),fs(i_suj).dipmom(1),fs(i_suj).dipmom(2),fs(i_suj).dipmom(3),.1,'color',colors('red'),'linewidth',1,'maxheadsize',.5,'autoscale','on')
  scatter3(fs(i_suj).ctxsm.pos(imax,1),fs(i_suj).ctxsm.pos(imax,2),fs(i_suj).ctxsm.pos(imax,3))

  %%
  %visualisation inflated

  figure(2); clf

  m = mean(source1.avg.pow(:,1:n_pts), 2);
  ft_plot_mesh(fs(i_suj).ctxsm_inflated, 'vertexcolor', m);
  view([180 0]); h = light; set(h, 'position', [0 1 0.2]); lighting gouraud; material dull
  hold on
  % quiver3(fs(i_suj).dippos(1,1),fs(i_suj).dippos(1,2),fs(i_suj).dippos(1,3),fs(i_suj).dipmom(1),fs(i_suj).dipmom(2),fs(i_suj).dipmom(3),.1,'color',colors('red'),'linewidth',1,'maxheadsize',.5,'autoscale','on')
  quiver3(fs(i_suj).ctxsm_inflated.pos(i_s,1),fs(i_suj).ctxsm_inflated.pos(i_s,2),fs(i_suj).ctxsm_inflated.pos(i_s,3),fs(i_suj).dipmom(1),fs(i_suj).dipmom(2),fs(i_suj).dipmom(3),.1,'color',colors('red'),'linewidth',1,'maxheadsize',.5,'autoscale','on')
  [~, imax] = max(m);
  scatter3(fs(i_suj).ctxsm_inflated.pos(imax,1),fs(i_suj).ctxsm_inflated.pos(imax,2),fs(i_suj).ctxsm_inflated.pos(imax,3))

  %%

  % figure(2);clf
  %
  % plot(source1.time, source1.avg.pow(2000,:));
  %
  % hold on
  %
  % %%
  %
  % figure(3);clf
  %
  % plot(tlck.time, tlck.avg);

  %%
  %statistiques

  DLE(ntris) = f_dle(source1.pos,source1.avg.pow, i_s);
  OA = f_oa(source1.pos,source1.avg.pow, i_s, n_source);
  SD = f_sd(source1.pos,source1.avg.pow, i_s, n_source);
end

% DLEinf = f_dle(fs(i_suj).ctxsm_inflated.pos,source1.avg.pow, i_s);
% OAinf = f_oa(fs(i_suj).ctxsm_inflated.pos,source1.avg.pow, i_s, n_source);
% SDinf = f_sd(fs(i_suj).ctxsm_inflated.pos,source1.avg.pow, i_s, n_source);

