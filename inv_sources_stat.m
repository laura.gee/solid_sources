
p_setpath
load layout
%%
fs = p_dbload('restin',{'name','label','headmodel','ds','ctxsm','grad','hm','ctxsm_inflated'});
load(fullfile(datadir,'alldata_50timepoints.mat'))
alldata = reshape(alldata, [248,50*200,89]);
%%
alldata = reshape(alldata, [248, 200, 50, 89]);
lfdir = fullfile(datadir,'lf_32k');
amp = 10;
t_min = -.2;
t_max = 1;
n_pts = 200;
n_source = 8004; %pb

i_suj = 2;
%i_s = 1800; %1:size(fs(1).ctxsm.pos,1)
%redisp(i_s)
 n_tris = [2 5 7 10 12 15 20 25 30 35 40 45 50]; 
%n_tris = [2 10 20 50];

DLE = NaN(n_source,numel(n_tris));
OA = NaN(n_source,numel(n_tris));
SD = NaN(n_source,numel(n_tris));
%%
      % load le leadfield precomputé pour un sujet au hasard
      load(fullfile(lfdir,['lf_32k_' fs(i_suj).ds '.mat']))
for i_s = 1:n_source %pb
    for i_ntri = 1:numel(n_tris)
      n_tri = n_tris(i_ntri);
      %%
      % créer une data structure de données "timelocked"

      data = [];
      data.label = fs(i_suj).label;
      data.grad = fs(i_suj).grad;
      fs(i_suj).dippos = fs(i_suj).ctxsm.pos(i_s,:);
      fs(i_suj).dipmom = fs(i_suj).ctxsm.nrm(i_s,:)';
      fs(i_suj).dipmom = fs(i_suj).dipmom ./ norm(fs(i_suj).dipmom);
      % take leadfield
      fs(i_suj).lf = leadfield.leadfield{i_s}(chnb(fs(i_suj).label,leadfield.label),:);


      lftopo1 = fs(i_suj).lf * fs(i_suj).dipmom * amp;

      for i_tri = 1:n_tri


        data.trial{i_tri} = alldata(:, 1:n_pts, i_tri, i_suj);
        data.trial{i_tri}(:, 1:n_pts) = data.trial{i_tri}(:, 1:n_pts) + lftopo1 ;
        data.time{i_tri} = linspace(t_min, t_max, n_pts);    %f_e=508.6275Hz


      end


      data.dimord = 'chan_time';
      %data.avg = lftopo1;

      cfg = [];
      cfg.covariance = 'yes';
      cfg.covariancewindow = [-.2 1]; %it will calculate the covariance matrix
      % on the timepoints that are
      % before the zero-time point in the
      % trials
      tlck = ft_timelockanalysis(cfg, data);

      %%
      %inverse model


      cfg               = [];
      cfg.method        = 'mne';
      cfg.sourcemodel   = leadfield;
      cfg.headmodel     = fs(i_suj).headmodel;
      cfg.mne.prewhiten = 'yes';
      cfg.mne.lambda    = 3;
      cfg.mne.scalesourcecov = 'yes';

      source1          = ft_sourceanalysis(cfg,tlck);


      %%
      %statistiques

      DLE(i_s,i_ntri) = f_dle(source1.pos,source1.avg.pow, i_s);    %pb
      OA(i_s,i_ntri) = f_oa(source1.pos,source1.avg.pow, i_s);
      SD(i_s,i_ntri) = f_sd(source1.pos,source1.avg.pow, i_s);
      
     
      
    end


end

%%
%stat

clean_DLE = rmmissing(DLE);
clean_OA = rmmissing(OA);
clean_SD = rmmissing(SD);

DLE_mean = mean(clean_DLE);
OA_mean = mean(clean_OA);
SD_mean = mean(clean_SD);

DLE_std_h = DLE_mean + std(clean_DLE);
DLE_std_l = DLE_mean - std(clean_DLE);
DLE_Q = quantile(clean_DLE,3);

OA_std_h = OA_mean + std(clean_OA);
OA_std_l = OA_mean - std(clean_OA);
OA_Q = quantile(clean_OA,3);

SD_std_h = SD_mean + std(clean_SD);
SD_std_l = SD_mean - std(clean_SD);
SD_Q = quantile(clean_SD,3);

%%
%Recherche min

[min_DLE i_min_DLE] = min(DLE);
[min_SD i_min_SD] = min(SD);
[max_DLE i_max_DLE] = max(DLE);
[max_SD i_max_SD] = max(SD);


%%
%plot

figure(1)
plot(n_tris,DLE_mean,'b');
hold on
plot(n_tris,DLE_Q,'c');
hold on
plot(n_tris,DLE_std_h,n_tris,DLE_std_l);
hold on
plot(n_tris,min_DLE,'--',n_tris,max_DLE,'--');
fill([n_tris,fliplr(n_tris)],[DLE_std_h, fliplr(DLE_std_l)],'b','FaceAlpha',.05);
title('DLE');
xlabel('number of trials');
ylabel('DLE value');


% figure(2)
% plot(n_tris,OA_mean,'r');
% hold on
% plot(n_tris,OA_Q,'m');
% hold on
% plot(n_tris,OA_std_h,n_tris,OA_std_l);
% fill([n_tris,fliplr(n_tris)],[OA_std_h, fliplr(OA_std_l)],'r','FaceAlpha',.05);
% title('OA');
% xlabel('number of trials');
% ylabel('OA value');

figure(3)
plot(n_tris,SD_mean,'g');
hold on
plot(n_tris,SD_Q,'c');
hold on
plot(n_tris,SD_std_h,n_tris,SD_std_l);
hold on
plot(n_tris,min_SD,'--',n_tris,max_SD,'--');
fill([n_tris,fliplr(n_tris)],[SD_std_h, fliplr(SD_std_l)],'g','FaceAlpha',.05);
title('SD');
xlabel('number of trials');
ylabel('SD value');

% 
% figure(4)
% subplot(2,3,1)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', DLE(:,1));
% title('ntris=2')
% caxis manual
% caxis([0 0.1]);
% colorbar;
% subplot(2,3,2)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', DLE(:,2));
% title('ntris=5')
% caxis manual
% caxis([0 0.1]);
% colorbar;
% subplot(2,3,3)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', DLE(:,4));
% title('ntris=10')
% caxis manual
% caxis([0 0.1]);
% colorbar;
% subplot(2,3,4)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', DLE(:,7));
% title('ntris=20')
% caxis manual
% caxis([0 0.1]);
% colorbar;
% subplot(2,3,5)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', DLE(:,9));
% title('ntris=30')
% caxis manual
% caxis([0 0.1]);
% colorbar;
% subplot(2,3,6)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', DLE(:,13));
% title('ntris=50')
% caxis manual
% caxis([0 0.1]);
% colorbar;
% 
% 
% figure(5)
% subplot(2,3,1)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', SD(:,1));
% title('ntris=2')
% caxis manual
% caxis([0.12 0.25]);
% colorbar;
% subplot(2,3,2)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', SD(:,2));
% title('ntris=5')
% caxis manual
% caxis([0.12 0.25]);
% colorbar;
% subplot(2,3,3)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', SD(:,4));
% title('ntris=10')
% caxis manual
% caxis([0.12 0.25]);
% colorbar;
% subplot(2,3,4)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', SD(:,7));
% title('ntris=20')
% caxis manual
% caxis([0.12 0.25]);
% colorbar;
% subplot(2,3,5)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', SD(:,9));
% title('ntris=30')
% caxis manual
% caxis([0.12 0.25]);
% colorbar;
% subplot(2,3,6)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', SD(:,13));
% title('ntris=50')
% caxis manual
% caxis([0.12 0.25]);
% colorbar;

% figure(4)
% subplot(2,3,1)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', DLE(:,1));
% title('ntris=2')
% caxis manual
% caxis([0 0.1]);
% colorbar;
% view(30,45);
% subplot(2,3,2)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', DLE(:,2));
% title('ntris=5')
% caxis manual
% caxis([0 0.1]);
% colorbar;
% view(30,45);
% subplot(2,3,3)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', DLE(:,4));
% title('ntris=10')
% caxis manual
% caxis([0 0.1]);
% colorbar;
% view(30,45);
% subplot(2,3,4)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', DLE(:,7));
% title('ntris=20')
% caxis manual
% caxis([0 0.1]);
% colorbar;
% view(30,45);
% subplot(2,3,5)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', DLE(:,9));
% title('ntris=30')
% caxis manual
% caxis([0 0.1]);
% colorbar;
% view(30,45);
% subplot(2,3,6)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', DLE(:,13));
% title('ntris=50')
% caxis manual
% caxis([0 0.1]);
% colorbar;
% view(30,45);
% 
% 
% figure(5)
% subplot(2,3,1)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', SD(:,1));
% title('ntris=2')
% caxis manual
% caxis([0.12 0.25]);
% colorbar;
% view(30,45);
% subplot(2,3,2)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', SD(:,2));
% title('ntris=5')
% caxis manual
% caxis([0.12 0.25]);
% colorbar;
% view(30,45);
% subplot(2,3,3)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', SD(:,4));
% title('ntris=10')
% caxis manual
% caxis([0.12 0.25]);
% colorbar;
% view(30,45);
% subplot(2,3,4)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', SD(:,7));
% title('ntris=20')
% caxis manual
% caxis([0.12 0.25]);
% colorbar;
% view(30,45);
% subplot(2,3,5)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', SD(:,9));
% title('ntris=30')
% caxis manual
% caxis([0.12 0.25]);
% colorbar;
% view(30,45);
% subplot(2,3,6)
% ft_plot_mesh(fs(i_suj).ctxsm, 'vertexcolor', SD(:,13));
% title('ntris=50')
% caxis manual
% caxis([0.12 0.25]);
% colorbar;
% view(30,45);
% 
% 
% 
