
p_setpath

fs = p_dbload('restin',{'ds','ctxsm','grad','hm'});
load(fullfile(datadir,'alldata_50timepoints.mat'))
%%
lfdir = fullfile(datadir,'lf_32k');
mymkdir(lfdir)
%%
for i = 1:numel(fs)
  %%
  sourcemodel = fs(i).ctxsm;
  headmodel = fs(i).hm;
  grad = fs(i).grad;

  cfg         = [];
  cfg.grad    = grad;   % sensor information
  cfg.channel = grad.label;  % the used channels
  cfg.sourcemodel = sourcemodel;   % source points
  cfg.headmodel = headmodel;   % volume conduction model
  cfg.singleshell.batchsize = 1e4; % speeds up the computation

  leadfield   = ft_prepare_leadfield(cfg);

  save(fullfile(lfdir,['lf_32k_' fs(i).ds]),'leadfield','-nocompression');
end