function MCs = p_create_MCs(fs,nMC,nsujs,ntris,maxntris,ntests,filename)

% MCs = p_create_MCs(fs,nMC,nsujs,ntris,maxntris,ntests,filename)
%
% creates a convenient structure with a number of random draws of trials
% and subjects according to what's available in fs
%

if not(exist('fs','var'))
    fs = evalin('caller','fs');
end
if not(exist('nMC','var'))
    nMC = 200;
end
if not(exist('nsujs','var'))
    nsujs = 10:10:50;
end
if not(exist('ntris','var'))
    ntris = 10:10:100;
end
if not(exist('maxntris','var'))
    maxntris = max(ntris);
end
if not(exist('ntests','var'))
    ntests = 1;
end

MCs = struct('sujs',{},'tris',{},'m',{},'h',{},'p',{},'tstat',{},'df',{},'sd',{});

sample_trials_with_replacement = true;

for itest = 1:ntests
    for insujs = 1:numel(nsujs)
        for intris = 1:numel(ntris)
            for iMC = 1:nMC
                MCs(insujs,intris,iMC,itest).sujs = randsample(1:size(fs,1),nsujs(insujs));
                for isuj = 1:numel(MCs(insujs,intris,iMC).sujs)
                    MCs(insujs,intris,iMC,itest).tris(isuj,:) = randsample(1:maxntris,ntris(intris),sample_trials_with_replacement);
                end
            end
        end
    end
end

if exist('filename','var')
    save(filename,'MCs','fs');
end
