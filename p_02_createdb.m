% this script creates a database with all useful files for subsequent
% computations.
% data
% head and source model
% atlas
% super highres source model for figures
% NB: for super highres, need to use connectome_wb and data from the
% structural pipeline. See script called resample_surf.sh

p_setpath
addpath(fullfile(codedir,'solid_MEEG/megconnectome/analysis_functions'))
figdir = fullfile(figsdir,'Figures_ctx');
if not(exist(figdir,'dir'))
    mkdir(figdir)
end


%% list all files.
% all
allfs = flister('.*','dir',rawdir);
% *alltrials.mat: my resaved rmegpreproc.mat data
fs = flister('(?<ds>\d{6})_alltrials.mat','list',{allfs.name});
fs = flist_consolidate(fs);

% HCP release: MEG anatomy
% all head model: single shell BEM
hs = flister('(?<ds>\d{6}).*MEG_anatomy_(?<hs>headmodel).mat$','list',{allfs.name});
hs = flist_consolidate(hs);
% surface (4k): cortex (midthickness), 4000 vertices per hemi
ss = flister('(?<ds>\d{6}).*(?<LR>L|R).midthickness.4k_fs_LR.surf.gii','list',{allfs.name});
ss = flist_consolidate(ss);
ss_inflated = flister('(?<ds>\d{6}).*(?<LR>L|R).inflated.4k_fs_LR.surf.gii','list',{allfs.name});
ss_inflated = flist_consolidate(ss_inflated);

% HCP release: MRI structural preproc
% surfaces and labels (32k): in order to map labels to 4k surf
labs = flister('(?<ds>\d{6}).(?<LR>L|R).aparc.a2009s.32k_fs_LR.label.gii','list',{allfs.name});
labs = flist_consolidate(labs);
geoms = flister('(?<ds>\d{6}).(?<LR>L|R).midthickness.32k_fs_LR.surf.gii','list',{allfs.name});
geoms = flist_consolidate(geoms);
% surfaces and labels (164k) (normalized or not, for comparison)
% this is only for figure 2
labs164 = flister('(?<transfo>T1w|NonLinear|NonLinear/Native)/(?<ds>\d{6}).(?<LR>L|R).aparc.a2009s.(164k_fs_LR|native).label.gii','list',{allfs.name});
labs164 = flist_consolidate(labs164);
labs164(4,:) = labs164(2,:);
geoms164 = flister('(?<transfo>T1w|NonLinear|T1w/Native|NonLinear/Native)/(?<ds>\d{6}).(?<LR>L|R).midthickness.(164k_fs_LR|native).surf.gii','list',{allfs.name});
geoms164 = flist_consolidate(geoms164);

%% basic db: file names, head model, source model, etc.
warning('off','MATLAB:unknownElementsNowStruc');

for i_f = 1:size(fs,1)
    disp(['====== ' num2str(i_f) ' ' fs(i_f).name])
    % store file names
    fs(i_f).headmodel = hs(regexpcell({hs(:,1).name},fs(i_f).ds),1).name;
    fs(i_f).Lsurf = ss(regexpcell({ss(:,1).name},fs(i_f).ds),1).name;
    fs(i_f).Rsurf = ss(regexpcell({ss(:,2).name},fs(i_f).ds),2).name;
    fs(i_f).Lsurf_inflated = ss_inflated(regexpcell({ss_inflated(:,1).name},fs(i_f).ds),1).name;
    fs(i_f).Rsurf_inflated = ss_inflated(regexpcell({ss_inflated(:,2).name},fs(i_f).ds),2).name;
    
    % load headmodel
    tmp = load(fs(i_f).headmodel);
    hm = tmp.headmodel;
    hm = ft_convert_units(hm,'m');
        
    % load some data to recover gradiometer and channel order
    tmp = load(fs(i_f).name,'data');
    % load gradiometer
    grad = tmp.data.grad;
    grad = ft_convert_units(grad,'m');
    % channel name and orders (label)
    fs(i_f).label = tmp.data.label;
    
    % prepare headmodel and gradiometer structure for later use with
    % compute_leadfield (in other scripts)
    [hm,grad] = ft_prepare_vol_sens(hm,grad);
    
    % read and save trasform (we will use spm2bti).
    % note this transform applies to mm unit so all has to be changed
    % to mm before applying
    tmp = hcp_read_ascii(strrep(fs(i_f).headmodel,'headmodel.mat','transform.txt'));
    fs(i_f).transform = tmp.transform;
    
    % merge L and R ctx surfaces
    headL=ft_read_headshape(fs(i_f).Lsurf);
    headR=ft_read_headshape(fs(i_f).Rsurf);
    ctxsm=headL;
    ctxsm.pos=[headL.pos ;headR.pos];
    ctxsm.tri=[headL.tri ; headR.tri + size(headL.pos,1)];
    ctxsm = ft_convert_units(ctxsm, 'mm');
    ctxsm = ft_transform_geometry(fs(i_f).transform.spm2bti, ctxsm);
    % compute normals
    ctxsm.nrm = normals(ctxsm.pos, ctxsm.tri);
    
    % merge L and R inflated ctx surfaces
    headL=ft_read_headshape(fs(i_f).Lsurf_inflated);
    headR=ft_read_headshape(fs(i_f).Rsurf_inflated);
    ctxsm_inflated=headL;
    ctxsm_inflated.pos=[headL.pos ;headR.pos];
    ctxsm_inflated.tri=[headL.tri ; headR.tri + size(headL.pos,1)];
    ctxsm_inflated = ft_convert_units(ctxsm_inflated, 'mm');
    ctxsm_inflated = ft_transform_geometry(fs(i_f).transform.spm2bti, ctxsm_inflated);

    % store all
    fs(i_f).hm = hm;
    fs(i_f).grad = grad;
    fs(i_f).ctxsm = ft_convert_units(ctxsm,'m');
    fs(i_f).ctxsm_inflated = ft_convert_units(ctxsm_inflated,'m');
    
    % now for 32k ctx (for atlas)
    i_lab = strcmp({labs(:,1).ds},fs(i_f).ds);
    i_geom = strcmp({geoms(:,1).ds},fs(i_f).ds);
    % read labels and surfaces
    labels = hcp_read_atlas({labs(i_lab,:).name});
    highres_ctx = ft_read_headshape({geoms(i_geom,:).name});
    highres_ctx.unit = 'mm';
    % merge labels and ctx with hcp utility
    atlas = hcp_mergestruct(labels,highres_ctx);
    atlas.rgba = [atlas.rgba(:,1:3);atlas.rgba(:,1:3)];
    atlas = ft_transform_geometry(fs(i_f).transform.spm2bti, atlas);
    atlas = ft_convert_units(atlas,'m');
    % compute normals
    atlas.nrm = normals(atlas.pos, atlas.tri);
    
    % mapping atlas to lower res ctx (so all vertices are labeled in
    % lower res ctx)
    cfg = [];
    cfg.interpmethod = 'nearest';
    cfg.parameter = 'parcellation';
    tmp = ft_sourceinterpolate(cfg,atlas,fs(i_f).ctxsm);
    
    fs(i_f).ctxsm.parcellation = tmp.parcellation;
    fs(i_f).ctxsm.parcellationlabel = atlas.parcellationlabel;
    fs(i_f).ctxsm.rgba = [atlas.rgba(:,1:3);atlas.rgba(:,1:3)];
    
    fs(i_f).atlas32 = atlas;
    
    if 0 % if need to plot, set to 1
        %% plotting 4k and 32k atlas with labels
        ctxsm = fs(i_f,1).ctxsm;
        ctxsm_inflated = fs(i_f,1).ctxsm_inflated;
        ctxsm.rgba = ctxsm.rgba(1:150,:);
        figure(484);clf
        subplot(121);cla
        ft_plot_mesh(ctxsm,'vertexcolor',fs(i_f,1).ctxsm.rgba(ctxsm.parcellation,:),'facealpha',1)%,'edgecolor','k'
        hold on
        ft_plot_mesh(ctxsm_inflated,'vertexcolor',fs(i_f,1).ctxsm.rgba(ctxsm.parcellation,:),'facealpha',.5)%,'edgecolor','k'
        set(gca,'UserData',ctxsm);
        h = datacursormode(gcf);
        set(h,'updatefcn',@test_atlas_cb);
        
        lighting gouraud
        material dull
        camlight
        
        subplot(122);cla
        ft_plot_mesh(atlas,'vertexcolor',atlas.rgba(atlas.parcellation,:),'facealpha',1)
        
        set(gca,'UserData',atlas);
        h = datacursormode(gcf);
        set(h,'updatefcn',@test_atlas_cb);
        
        lighting gouraud
        material dull
        camlight
    end
end
%%
p_dbsave(fs,'restin');

%% now high res cortex
for i_f = 1:size(fs,1)
    %%
    i_lab = find(strcmp({labs164(1,:,1).ds},fs(i_f,1).ds));
    i_geom = find(strcmp({geoms164(1,:,1).ds},fs(i_f,1).ds));
    
    labels = {};highres_ctx = {}; atlas164 = {};
    for itrans = 1:4
        labels{itrans} = hcp_read_atlas({labs164(itrans,i_lab,:).name});
        highres_ctx{itrans} = ft_read_headshape({geoms164(itrans,i_geom,:).name});
        highres_ctx{itrans}.unit = 'mm';
        
        atlas164{itrans} = hcp_mergestruct(labels{itrans},highres_ctx{itrans});
        atlas164{itrans}.rgba = [atlas164{itrans}.rgba(:,1:3);atlas164{itrans}.rgba(:,1:3)];
        atlas164{itrans}.unit = 'mm';
        
        atlas164{itrans} = ft_transform_geometry(fs(i_f,1).transform.spm2bti, atlas164{itrans});
        atlas164{itrans} = ft_convert_units(atlas164{itrans},'m');
    end
    
    % This is the one we keep : /T1w/$suj.L.midthickness.164k_fs_LR.surf.gii
    % created with fullfile(codedir,'resample_surf.sh)
    fs(i_f,1).atlas164 = atlas164{3};
    
    if 0
        %%
        ctxsm = fs(i_f,1).ctxsm;
        ctxsm.rgba = ctxsm.rgba(1:150,:);
        fig(23,1515);
        
        subplot(231);cla
        ft_plot_mesh(ctxsm,'vertexcolor',fs(i_f,1).ctxsm.rgba(ctxsm.parcellation,:),'facealpha',1)%,'edgecolor','k'
        set(gca,'UserData',ctxsm);
        h = datacursormode(gcf);
        set(h,'updatefcn',@test_atlas_cb);
        
        view(0,0)
        title('low res (MEG pipeline)')
        lighting gouraud
        material dull
        camlight
        
        for itrans = 1:4
            subplot(2,3,itrans+1);cla
            ft_plot_mesh(atlas164{itrans},'vertexcolor',atlas164{itrans}.rgba(atlas164{itrans}.parcellation,:),'facealpha',1)
            
            set(gca,'UserData',atlas164{itrans});
            h = datacursormode(gcf);
            set(h,'updatefcn',@test_atlas_cb);
            
            view(0,0)
            t = fileparts(geoms164(itrans,i_geom,1).name);
            t(1:strfind(t,geoms164(itrans,i_geom,1).ds)-1) = [];
            title(t)
            lighting gouraud
            material dull
            camlight
        end
        set(gcf,'paperpositionmode','auto')
        print('-dpng','-r300',fullfile(figdir, [fs(i_f).ds,'.surf.Figure.png']))
        
        
    end
end
p_dbsave(fs,'restin','atlas164');
%%


function output_txt = test_atlas_cb(obj,event_obj)
% Display the position of the data cursor
% obj          Currently not used (empty)
% event_obj    Handle to event object
% output_txt   Data cursor text string (string or cell array of strings).

pos = get(event_obj,'Position');
output_txt = {['X: ',num2str(pos(1),4)],...
    ['Y: ',num2str(pos(2),4)]};

% If there is a Z-coordinate in the position, display it as well
if length(pos) > 2
    output_txt{end+1} = ['Z: ',num2str(pos(3),4)];
end
tmp = get(gca,'userdata');
ctxsm = tmp{1};vtx = tmp{2};
if not(isfield(ctxsm,'areas'))
    ctxsm.areas.parcels = {};
    ctxsm.areas.coords = [];
end
try
    output_txt(end+1) = strrep(ctxsm.parcellationlabel(ctxsm.parcellation(dsearchn(ctxsm.pos,pos))),'_',' ');
    ctxsm.areas.parcels = [ctxsm.areas.parcels output_txt(end)];
    ctxsm.areas.coords = [ctxsm.areas.coords; pos(1:3)];
    if not(isfield(ctxsm,'nrm'))
        % compute normals
        ctxsm.nrm = normals(ctxsm.pos, ctxsm.tri);
    end
    set(gca,'userdata',{ctxsm vtx});
    idx = dsearchn(ctxsm.pos,pos);
    dippos = ctxsm.pos(idx,:);
    dipmom = ctxsm.nrm(idx,:);
    output_txt{end+1} = num2str(idx);
    hold on
    try
        delete(findobj(gca,'tag','dip'))
    end
    h = quiver3(dippos(1),dippos(2),dippos(3),dipmom(1)/10,dipmom(2)/10,dipmom(3)/10,'r','linewidth',2,'maxheadsize',2);
    set(h,'tag','dip')
    output_txt{end+1} = num2str(vtx(idx));
end
todisp = regexprep(output_txt,'[XYZ]: ','');
todisp = sprintf('%s    ',todisp{:});
disp(todisp)
end