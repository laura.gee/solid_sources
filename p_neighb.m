%% create neighbourhood matrix
cfg = [];
cfg.method = 'template';
cfg.template = 'bti248_neighb.mat';
neighbours = ft_prepare_neighbours(cfg);
labels = fs(1).label;
channeighbstructmat = zeros(numel(labels));
for i = 1:numel(neighbours)
    ichan = chnb(neighbours(i).label,labels);
    ineighb = chnb(neighbours(i).neighblabel,labels);
    for ii = 1:numel(ineighb)
        channeighbstructmat(ichan,ineighb(ii)) = 1;
    end
end
if 0 % plot neighbourhood in space
    %%
    cfg = [];
    cfg.neighbours = neighbours;
    cfg.grad = fs(1).grad;
    ft_neighbourplot(cfg)
    figure(45);clf
    imagesc(channeighbstructmat)
    xticklabels(labels(xticks))
    yticklabels(labels(yticks))
end