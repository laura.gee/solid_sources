function [DLE] = f_dle(pos,pow,i_source)
% compute dipole localisation error
% input : pos = position of all sources
%         pow = signal at all source positions
%         i_source = index of source of interest in pos and pow

%%
%find max signal index 
pow_avg = mean(pow,2);
[~, i_peak_signal] = max(pow_avg);


%%
%find pos associated to i_peak_signal
pos_peak_signal = pos(i_peak_signal, :);
%%
%find pos of the source of interest

pos_source = pos(i_source, :);

%%
%calculate dipole localisation error

DLE = norm(pos_peak_signal-pos_source);

end


