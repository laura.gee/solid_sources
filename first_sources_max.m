
p_setpath
load layout

fs = p_dbload('restin',{'name','label','headmodel','ds','ctxsm','grad','hm'});
load(fullfile(datadir,'alldata_50timepoints.mat'))
alldata = reshape(alldata, [248,50*200,89]);
%%
alldata = reshape(alldata, [248, 200, 50, 89]);
lfdir = fullfile(datadir,'lf_32k');

% load le leadfield precomputé pour un sujet au hasard
load(fullfile(lfdir,['lf_32k_' fs(1).ds '.mat']))
%lfsingdir = fullfile(lfdir,['lf_32k_' fs(1).ds]);
%lfsing = leadfield(1); --> inutile
amp = 10;
t_min = -.2;
t_max = 1;
% f_e = 508.6275;
% T_e = 1/f_e;
% n_pts = (t_max-t_min)/T_e;
n_pts=200;
%%
%%
% créer une data structure de données "timelocked"

for i_s = 2000 %1:size(fs(1).ctxsm.pos,1)
  data = [];
  data.label = fs(1).label;
  data.grad = fs(1).grad;
  for i_tri = 1:50
    fs(1).dippos = fs(1).ctxsm.pos(i_s,:);
    fs(1).dipmom = fs(1).ctxsm.nrm(i_s,:)';
    fs(1).dipmom = fs(1).dipmom ./ norm(fs(1).dipmom);
    % take leadfield
    fs(1).lf = leadfield.leadfield{i_s}(chnb(fs(1).label,leadfield.label),:);


    lftopo1 = fs(1).lf * fs(1).dipmom * amp;


    %if 0

    %end

    data.trial{i_tri} = alldata(:, 1:n_pts, i_tri, 1);
    data.trial{i_tri}(:, 150:200) = data.trial{i_tri}(:, 150:200) + lftopo1;
    data.time{i_tri} = linspace(t_min, t_max, n_pts);    %f_e=508.6275Hz


  end
  data.dimord = 'chan_time';
  %data.avg = lftopo1;

  cfg = [];
  cfg.covariance = 'yes';
  cfg.covariancewindow = [-.2 1]; %it will calculate the covariance matrix
  % on the timepoints that are
  % before the zero-time point in the
  % trials
  tlck = ft_timelockanalysis(cfg, data);
  %tlck.grad = fs(1).grad;

  %%
  %inverse model


  cfg               = [];
  cfg.method        = 'mne';
  cfg.sourcemodel   = leadfield;
  cfg.headmodel     = fs(1).headmodel;
  cfg.mne.prewhiten = 'yes';
  cfg.mne.lambda    = 3;
  cfg.mne.scalesourcecov = 'yes';

  source1          = ft_sourceanalysis(cfg,tlck);

  %%
  %visualisation

  figure(1); clf

  m = mean(source1.avg.pow(:,150:200), 2);
  ft_plot_mesh(source1, 'vertexcolor', m);
  view([180 0]); h = light; set(h, 'position', [0 1 0.2]); lighting gouraud; material dull
  hold on
  quiver3(fs(1).dippos(1,1),fs(1).dippos(1,2),fs(1).dippos(1,3),fs(1).dipmom(1),fs(1).dipmom(2),fs(1).dipmom(3),.1,'color',colors('red'),'linewidth',1,'maxheadsize',.5,'autoscale','on')

  %%

  figure(2);clf

  plot(source1.time, source1.avg.pow(2000,:));

  hold on

  %%

  figure(3);clf

  plot(tlck.time, tlck.avg);

end
