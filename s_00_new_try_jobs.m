function s_00_new_try_jobs(ijob)

% for all sources of the the superhigh resolution mesh, take nsubj, ntri,
% compute a simple, inject signal, simple t-test, store result value

% preparing environment
% codedir = '/network/lustre/iss02/cenir/analyse/meeg/SOLID_SOURCES/jobs/00_first_try/amp_10/scripts';
codedir = '/DATA_FAST/DATA/SOLID_SOURCES/jobs/00_new_try/amp_10/scripts';
addpath(codedir)
p_setpath_loc

load('../../common.mat','allctxsm','alldata','allgrad');
load('../clusterjob_common.mat','fs','specif')
[fs.ctxsm] = rep2struct(allctxsm);
[fs.grad] = rep2struct(allgrad);

struct2ws(specif)

load(sprintf('../clusterjob%03d.mat',ijob),'MCs')

t_min = -.2;
t_max = 1;
n_pts = 200;
%% start looping through sources
%for i_s = 1:size(fs(1).ctxsm.pos,1)
for i_s = 1:3
    %         tic
    if isfield(fs,'lf')
        fs = rmfield(fs,'lf');
    end
    %% load precomputed leadfield
    load(fullfile(datadir,'ctxsm_all_lf_32k/','Native_32k_fs_LR',num2str(floor(i_s/1000),'%03d'),['lf_' num2str(i_s,'%06d')]),'lf');
    % now for each subject
    % we stack leadfields in the fs structure
    for isuj = 1:size(fs,1)
        % mark dipole position
        fs(isuj).dippos = fs(isuj).ctxsm.pos(i_s,:);
        fs(isuj).dipmom = fs(isuj).ctxsm.nrm(i_s,:)';
        fs(isuj).dipmom = fs(isuj).dipmom ./ norm(fs(isuj).dipmom);
        % take leadfield
        fs(isuj).lf = lf(:,:,isuj);
        
    end

    
    nchan = size(alldata,1);
    ntimes = specif.times;%size(alldata,2);
    fprintf('source%06d\n',i_s)
    % now for all of the nMC data resamples
    for iMC = 1:numel(MCs)
        % find maxsensor across subjects of this MC
        clear alldipdat
        for i = 1:numel(MCs(iMC).sujs)
            alldipdat(:,:,i) = fs(MCs(iMC).sujs(i)).lf * fs(MCs(iMC).sujs(i)).dipmom * amp;
        end
        %[~,maxsensor] = maxabs(mean(alldipdat,3));
        % dimensions:
        % sensors, time, subjects;
        %         tic
        d1 = NaN([nchan,ntimes,size(MCs(iMC).tris,2)/2,numel(MCs(iMC).sujs)]);
        d2 = NaN(size(d1));
        % fill the data
        source_clean = [];
        for isuj = 1:numel(MCs(iMC).sujs)
            
            itri = 1:size(MCs(iMC).tris,2)/2;% first half of trials
            tmp = alldata(:,1:MCs(iMC).times,MCs(iMC).tris(isuj,itri),MCs(iMC).sujs(isuj));
            
            d1(:,:,:,isuj) = tmp;
            
            itri = (size(MCs(iMC).tris,2)/2+1):size(MCs(iMC).tris,2);% second half of trials
            tmp = alldata(:,1:MCs(iMC).times,MCs(iMC).tris(isuj,itri),MCs(iMC).sujs(isuj));
            
            d2(:,:,:,isuj) = tmp;
            
            % C'est ici qu'on insère la reconstruction de sources
            
            data = [];
            data.label = fs(isuj).label;
            data.grad = fs(isuj).grad;
            
            
            for itri = 1:size(d1,3)
                data.trial{itri} = d1(:,:,itri,isuj);
                data.trial{itri}(:, 1:n_pts) = data.trial{itri}(:, 1:n_pts) + alldipdat(:,:,isuj);
                data.time{itri} = linspace(t_min, t_max, n_pts);
            end
            
            data.dimord = 'chan_time';
            %data.avg = lftopo1;
            
            
            cfg = [];
            cfg.covariance = 'yes';
            cfg.covariancewindow = [-.2 1]; 
            tlck = ft_timelockanalysis(cfg, data);
            
            
            cfg               = [];
            cfg.method        = 'mne';
            cfg.sourcemodel   = fs(isuj).ctxsm;
%             cfg.sourcemodel.leadfield = XXX;
            cfg.headmodel     = fs(isuj).hm;
            cfg.mne.prewhiten = 'yes';
            cfg.mne.lambda    = 3;
            cfg.mne.scalesourcecov = 'yes';
            
            source1{isuj} = ft_sourceanalysis(cfg,tlck);
            source_clean.pow(:,isuj) = mean(source1{isuj}.avg.pow,2);
            source_clean.pos(:,:,isuj) = source1{isuj}.pos;
            
        end
        whosnan.pow = isnan(source_clean.pow);
        whosnan.pos = isnan(source_clean.pos);

        source_clean.pow = nanmean(source_clean.pow,2);
        source_clean.pos = nanmean(source_clean.pos,3);
        
        
        DLE(i_s,iMC) = f_dle(source_clean.pos,source_clean.pow, i_s);
        SD(i_s,iMC) = f_sd(source_clean.pos,source_clean.pow, i_s);
    end
    %%
end


save(sprintf('../clusterjob%03d.mat',ijob),'MCs','DLE','SD','whosnan')


% clean_DLE = rmmissing(DLE);
% clean_SD = rmmissing(SD);
% 
% DLE_mean = mean(clean_DLE);
% SD_mean = mean(clean_SD);
% 
% DLE_std_h = DLE_mean + std(clean_DLE);
% DLE_std_l = DLE_mean - std(clean_DLE);
% DLE_Q = quantile(clean_DLE,3);
% 
% SD_std_h = SD_mean + std(clean_SD);
% SD_std_l = SD_mean - std(clean_SD);
% SD_Q = quantile(clean_SD,3);
end