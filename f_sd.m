function [SD] = f_sd(pos,pow,i_source,n_j_source)
% compute dipole localisation error
% input : pos = position of all sources
%         pow = signal at all source positions
%         i_source = index of source of interest in pos and pow

%%
 
pos_i_source = pos(i_source, :);

num_SD = 0;
den_SD = 0;

del = isnan(pow);
pow(del) = 0;

    for j = 1:n_j_source
        
        %position de la source j
        pos_j_source = pos(j,:);

        %distance entre source j et source i
        dij = norm(pos_j_source - pos_i_source);

        %création de la variable Fij
        Fij = mean(pow(j,:));
        
        %calcul numérateur SD
        num_SD_j = dij*Fij^2;
        num_SD = num_SD + num_SD_j;

        %calcul dénominateur SD
        den_SD_j = Fij^2;
        den_SD = den_SD + den_SD_j;       

    end

%%
%calculate Spatial dispersion 

SD = sqrt(num_SD / den_SD);
return

end

